// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;

namespace GestureRecognizerTargetIssue
{
	[Register ("GestureRecognizerTargetIssueViewController")]
	partial class GestureRecognizerTargetIssueViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel _View { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (_View != null) {
				_View.Dispose ();
				_View = null;
			}
		}
	}
}
