﻿using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace GestureRecognizerTargetIssue
{
    public partial class GestureRecognizerTargetIssueViewController : UIViewController
    {
        public GestureRecognizerTargetIssueViewController (IntPtr handle) : base (handle)
        {
        }

        public override void DidReceiveMemoryWarning ()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning ();
            
            // Release any cached data, images, etc that aren't in use.
        }

        #region View lifecycle

        UITapGestureRecognizer _GestureRecognizer;

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();
            
            // Perform any additional setup after loading the view, typically from a nib.
            _GestureRecognizer = new UITapGestureRecognizer();
            _View.AddGestureRecognizer(_GestureRecognizer);


            var token = _GestureRecognizer.AddTarget(OnTapToDie);

            // removing it leaves some internal ParametrizeDispatch attached somehow
            _GestureRecognizer.RemoveTarget(token);
            // also this signature really isn't correct, it should be Action<UIGestureRecognizer> ???
            _GestureRecognizer.AddTarget(OnTapToDie);

        }

        public void OnTapToDie(NSObject x)
        {
        }

        public override void ViewWillAppear (bool animated)
        {
            base.ViewWillAppear (animated);
        }

        public override void ViewDidAppear (bool animated)
        {
            base.ViewDidAppear (animated);
        }

        public override void ViewWillDisappear (bool animated)
        {
            base.ViewWillDisappear (animated);
        }

        public override void ViewDidDisappear (bool animated)
        {
            base.ViewDidDisappear (animated);
        }

        #endregion
    }
}

